#ifndef _RBE2001_WIFI_H_
#define _RBE2001_WIFI_H_

#include "robot.hpp"

#include <WiFi.h>
#include <WiFiUdp.h>

#define MSG_BUFFER_SIZE 512

/*!
 * 	RBE2001 network control class.
 *
 * 	This class represents a simplified network control system for the
 * 	robot.
 */
class WifiRemote {

	private:

		/*!
		 * 	Pointer to robot.
		 *
		 * 	This pointer points to the robot that is to be controlled by this
		 * 	remote.
		 */
		Robot* robot;

		/*!
		 * 	Udp connection handler thing
		 */
		WiFiUDP udp;

		/*!
		 * 	Incoming Packet Buffer
		 */
		uint8_t msg_buffer[MSG_BUFFER_SIZE];

	public:

		/*!
		 *  Wifi control initializer.
		 *
		 *  This function will initialize the wireless control system.
		 *
		 *  @param robot	The robot to control
		 *  @param ssid		The SSID to connect to
		 *  @param password	The password to the network
		 */
		WifiRemote(Robot* robot, const char* ssid, const char* password);

		/*!
		 * 	Destructor.
		 *
		 * 	Don't even bother.
		 */
		~WifiRemote();

		/*!
		 * 	Check for incoming packets
		 */
		void check_mail();

};

#endif
