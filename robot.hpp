#ifndef _RBE2001_ROBOT_H_
#define _RBE2001_ROBOT_H_

#include "pidMotor.h"
#include <ESP32Servo.h>

/*!
 * 	Robot States
 */
enum RobotState {
	READY_TO_RUMBLE = 0,
	MOVE_TO_PICKUP = 1,
	READY_TO_GRAB = 2,
	GRABBING = 3,
	READY_TO_MOVE_TO_DROP_OFF = 4,
	MOVING_TO_DROP_OFF = 5,
	READY_TO_DROP_OFF = 6,
	DROPPING_OFF = 7
};

/*!
 * 	RBE2001 Main Robot Class.
 *
 * 	This class defines the mind of the robot.  All functions performed by the
 * 	robot are controlled from this class.
 */
class Robot {

	public:

		/*!
		 *  Robot State
		 */
		RobotState robot_state = READY_TO_RUMBLE;

		/// Sub-state
		int sub_state = 0;

		/// Left Motor
		PidMotor* pm1;
		/// Right Motor
		PidMotor* pm2;
		/// Arm Motor
		PidMotor* arm;

		/// Jaw Servo
		ESP32PWM* servo;

		/// Angle target
		char angle = 0;
		/// Position target
		char position = 0;

	public:

		/*!
		 * 	Robot Constructor.
		 *
		 * 	This constructor creates a new instance of the robot, initializes
		 * 	all of it's components, and gets it ready to impress nobody by
		 * 	driving directly off the edge of a table.  Of 100 nobles watching,
		 * 	0 were impressed.
		 */
		Robot();

		/*!
		 *  Robot Destructor.
		 *
		 *  This function does not need to be implemented, as destruction will
		 *  be handled by the floor.
		 */
		~Robot();

		/*!
		 * 	Robot Loop Method.
		 *
		 * 	This function is invoked repeatedly until the robot is dead.
		 */
		void loop();

		/*!
		 * 	Emergency Stop.
		 */
		void estop();

		/*!
		 * 	Emergency Go.
		 */
		void resume();

		/*!
		 * 	Approve an action
		 */
		void approve();

		/*!
		 * 	Start Command.
		 *
		 * 	LETS GET THIS PARTY STARTED WOOOOOO
		 */
		void start();


};

#endif
