#include "controlwindow.h"
#include "ui_controlwindow.h"

#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <memory.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <errno.h>
#include <stdlib.h>
#include <iostream>

#include <stdio.h>

#include <QMessageBox>

// Copied from SO
int resolvehelper(const char* hostname, int family, const char* service, sockaddr_storage* pAddr)
{
    int result;
    addrinfo* result_list = NULL;
    addrinfo hints = {};
    hints.ai_family = family;
    hints.ai_socktype = SOCK_DGRAM; // without this flag, getaddrinfo will return 3x the number of addresses (one for each socket type).
    result = getaddrinfo(hostname, service, &hints, &result_list);
    if (result == 0)
    {
        //ASSERT(result_list->ai_addrlen <= sizeof(sockaddr_in));
        memcpy(pAddr, result_list->ai_addr, result_list->ai_addrlen);
        freeaddrinfo(result_list);
    }

    return result;
}

ControlWindow::ControlWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ControlWindow)
{
    ui->setupUi(this);
}

ControlWindow::~ControlWindow()
{
    delete ui;
}

int sock = 0;
sockaddr_storage addrDest;



void ControlWindow::on_pushButton_clicked() {

    // Error box for generating errors
    QMessageBox errorbox;

    // Multi-operation result storage
    int result;

    // "Connect" to the esp32
    sock = socket(AF_INET, SOCK_DGRAM, 0);

    // Get the target address
    addrDest = {};
    const char* uip_addr = ui->ip_addr->text().toStdString().c_str();
    const char* uip_port = ui->ip_port->text().toStdString().c_str();
    result = resolvehelper(uip_addr, AF_INET, uip_port, &addrDest);
    if (result != 0)
    {
       int lasterror = errno;
       char buff[2000];
       sprintf(&buff[0], "Connection Error: %i", lasterror);
       errorbox.critical(nullptr,"Error:",buff);
       sock = 0;
       return;
    }

}

#define MESSAGE_MAX_LEN 128

void ControlWindow::on_pushButton_2_clicked()
{

    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    char message[MESSAGE_MAX_LEN];
    message[0] = 0xc1;
    int result = sendto(sock, message, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
    sprintf(&message[0], "SENT %i", result);
}

void ControlWindow::on_pushButton_3_clicked()
{
    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    char message[MESSAGE_MAX_LEN];
    message[0] = 0xc2;
    int result = sendto(sock, message, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
    sprintf(&message[0], "SENT %i", result);
}

void ControlWindow::on_pushButton_4_clicked()
{
    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    char message[MESSAGE_MAX_LEN];
    message[0] = 0xc3;
    int result = sendto(sock, message, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
    sprintf(&message[0], "SENT %i", result);
}

void ControlWindow::on_pushButton_5_clicked()
{
    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    char message[MESSAGE_MAX_LEN];
    message[0] = 0xc4;
    int result = sendto(sock, message, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
    sprintf(&message[0], "SENT %i", result);
}

struct __attribute__((packed)) setpid_msg {
    char msg_id;
    char pid_target;
    double p;
    double i;
    double d;
};

void ControlWindow::setpid(char target) {

    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    // Get PID
    const char* pstr;
    const char* istr;
    const char* dstr;
    switch(target) {
        case(0):
            pstr = ui->pid1p->text().toStdString().c_str();
            istr = ui->pid1i->text().toStdString().c_str();
            dstr = ui->pid1d->text().toStdString().c_str();
            break;
        case(1):
            pstr = ui->pid2p->text().toStdString().c_str();
            istr = ui->pid2i->text().toStdString().c_str();
            dstr = ui->pid2d->text().toStdString().c_str();
            break;
        case(2):
            pstr = ui->pid3p->text().toStdString().c_str();
            istr = ui->pid3i->text().toStdString().c_str();
            dstr = ui->pid3d->text().toStdString().c_str();
            break;

    }
    double p = atof(pstr);
    double i = atof(istr);
    double d = atof(dstr);
    char msg[1024];
    sprintf(msg, "P: %lf I: %lf D: %lf", p, i, d);
    //errorbox.critical(nullptr,"PID1",msg);
    union merge {
        char rawmsg[MESSAGE_MAX_LEN];
        setpid_msg nmsg;
    } merge;



    // Create message
    merge.nmsg.msg_id = 0xc5;
    merge.nmsg.pid_target = target;
    merge.nmsg.p = p;
    merge.nmsg.i = i;
    merge.nmsg.d = d;
    int result = sendto(sock, merge.rawmsg, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
}

void ControlWindow::on_pushButton_6_clicked()
{
    setpid(0);
}

void ControlWindow::on_pid1p_cursorPositionChanged(int arg1, int arg2)
{

}

void ControlWindow::on_pushButton_7_clicked()
{
    setpid(1);
}

void ControlWindow::on_pushButton_8_clicked()
{
    setpid(2);
}

void ControlWindow::on_pushButton_9_clicked()
{
    setpid(0);
    setpid(1);
    setpid(2);
}

void ControlWindow::on_pushButton_10_clicked()
{
    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    char message[MESSAGE_MAX_LEN];
    message[0] = 0xaf;
    int result = sendto(sock, message, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
    sprintf(&message[0], "SENT %i", result);
}

void ControlWindow::on_pushButton_11_clicked()
{
    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    char message[MESSAGE_MAX_LEN];
    message[0] = 0xd0;
    int result = sendto(sock, message, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
    sprintf(&message[0], "SENT %i", result);
}

void ControlWindow::on_pushButton_12_clicked()
{
    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    char message[MESSAGE_MAX_LEN];
    message[0] = 0xd1;
    int result = sendto(sock, message, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
    sprintf(&message[0], "SENT %i", result);
}

void ControlWindow::on_pushButton_13_clicked()
{
    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    char message[MESSAGE_MAX_LEN];
    message[0] = 0xe0;
    int result = sendto(sock, message, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
    sprintf(&message[0], "SENT %i", result);
}

void ControlWindow::on_pushButton_14_clicked()
{
    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    char message[MESSAGE_MAX_LEN];
    message[0] = 0xe1;
    int result = sendto(sock, message, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
    sprintf(&message[0], "SENT %i", result);
}

struct __attribute__((packed)) move_msg {
    char msg_id;
    char direction;
    double count;
};

void ControlWindow::on_pushButton_15_clicked()
{
    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    // Get the values
    double count = atof(ui->movecount->text().toStdString().c_str());
    char turn = ui->moveturn->isChecked();

    union merge {
        char rawmsg[MESSAGE_MAX_LEN];
        move_msg mmsg;
    } merge;

    merge.mmsg.msg_id = 0xf1;
    merge.mmsg.direction = turn;
    merge.mmsg.count = count;

    int result = sendto(sock, merge.rawmsg, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
    printf("Sent %i bytes\n", result);
}

struct __attribute__((packed)) pick_msg {
    char msg_id;
    char angle;
    char position;
};

void ControlWindow::on_pushButton_16_clicked()
{
    // Error box for generating errors
    QMessageBox errorbox;
    if(sock == 0) {
        errorbox.critical(nullptr,"Error:","Not Connected");
        return;
    }
    // Get selected items
    char angle = ui->cbangle->currentIndex();
    char position = ui->cbposition->currentIndex();
    // Create the union
    union merge {
        char rawmsg[MESSAGE_MAX_LEN];
        pick_msg pmsg;
    } merge;
    merge.pmsg.msg_id = 0xf0;
    merge.pmsg.angle = angle;
    merge.pmsg.position = position;
    // Send the command
    int result = sendto(sock, merge.rawmsg, MESSAGE_MAX_LEN, 0, (sockaddr*)&addrDest, sizeof(addrDest));
}
