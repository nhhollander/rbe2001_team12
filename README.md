# Team 12's Awesome Code Repo

This repository contains the code used to drive Team 12's robot ~~to drink~~.

## Information

Hello there professor/sa/ta/person!

You might be wondering right about now, "What's up with this?  Where's the template code?"  Well boy do I have a story to tell you.

This all started sometime around 11:30pm on the 12th night of October, in the year 2018.  It was at this point that something magical happened, something magical was released.  This mysterious substance would later come to be known as the "magic smoke", for it's ability to power electronic devices that would be nothing but inert rocks without it.

"Have no fear", one of the students cried, "There are spares, and we shal be on our way in no time at all!"

But his optimism quickly gave way to frustration, and eventually defeat.  Endless streams of errors written in a forgotten tongue streamed past, their true meaning lost upon the party's mortal minds.

"There must be another way" one said, desperately looking for someone who could provide an answer.  "There are dozens of people here, surely someone knows what is going on!  Surely someone can provide a solution!".  Unfortunately, like his friend before, he was quickly overcome by frusturation and defeat.  They sat silently about, the fountain of ideas, workarounds, and hacks that had once fueled their ambitions long dried up before them.

After a long pause, a voice spoke up.  "What if we started over.  Clearn, with a barren slate before us".  "Madness!" another exclaimed, daunted by the thought of wasting what precious time remained on such a seemingly frivilous task.  "We've tried everything else" replied another.  "It's our only hope".

And so they began anew.  First the skeleton of the program was built, states, loops, and cases came shortly after.  Before long higher order logic began to populate the bones, and by daybreak a lean new system was born.  It was far from perfect, riddled with bugs, and often unstable, but it worked just well enough.  The day dragged on, minutes gave way to hours, and slowly the program gained more and more features.  The ability to lift an arm.  To close a jaw.

Unfortunately, a complete success was not to be.  With the deadline fast approaching and bugs numbering in the milions, a compromise had to be made.  Fatal bugs were overlooked in favor of minor improvements, but the robot moved!  It rumbled to life, performing tasks unimpressive of a four year old, but tasks nonetheless!

And so, now you know the story behind this new bundle of bits and bytes, and the reason for it's being.  With that, I bid you adieu.
