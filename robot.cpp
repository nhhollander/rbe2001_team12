#include "robot.hpp"

// =================
// = CONFIGURATION =
// =================

#define MOTOR_L_FWD_PIN		14
#define MOTOR_L_REV_PIN 	27
#define MOTOR_L_ENC_A_PIN	34
#define MOTOR_L_ENC_B_PIN	35
#define MOTOR_L_MAX_SPEED	150

#define MOTOR_R_FWD_PIN		26
#define MOTOR_R_REV_PIN		25
#define MOTOR_R_ENC_A_PIN	33
#define MOTOR_R_ENC_B_PIN	32
#define MOTOR_R_MAX_SPEED	150

#define MOTOR_A_FWD_PIN		18
#define MOTOR_A_REV_PIN		19
#define MOTOR_A_ENC_A_PIN	13
#define MOTOR_A_ENC_B_PIN	17
#define MOTOR_A_MAX_SPEED	250

#define SERVO_CONTROL_PIN	16
#define SERVO_PWM_MIN		1000
#define SERVO_PWM_MAX		2000

// =============
// = FUNCTIONS =
// =============

#define PWMCHECK(c) \
	if(!ESP32PWM::hasPwm(c)) { \
		Serial.println("Error: Pin "#c" is NOT PWM\n"); \
		while(true); \
	}\

Robot::Robot() {
	// Sanity check
	PWMCHECK(MOTOR_L_FWD_PIN);
	PWMCHECK(MOTOR_L_REV_PIN);
	PWMCHECK(MOTOR_R_FWD_PIN);
	PWMCHECK(MOTOR_R_REV_PIN);
	PWMCHECK(SERVO_CONTROL_PIN);

	// Initialize the motors
	pm1 = new PidMotor(
			MOTOR_L_FWD_PIN, MOTOR_L_REV_PIN,
			MOTOR_L_ENC_A_PIN, MOTOR_L_ENC_B_PIN,
			-MOTOR_L_MAX_SPEED, MOTOR_L_MAX_SPEED);
	pm2 = new PidMotor(
			MOTOR_R_FWD_PIN, MOTOR_R_REV_PIN,
			MOTOR_R_ENC_A_PIN, MOTOR_R_ENC_B_PIN,
			-MOTOR_R_MAX_SPEED, MOTOR_R_MAX_SPEED);
	arm = new PidMotor(
			MOTOR_A_FWD_PIN, MOTOR_A_REV_PIN,
			MOTOR_A_ENC_A_PIN, MOTOR_A_ENC_B_PIN,
			-MOTOR_A_MAX_SPEED, MOTOR_A_MAX_SPEED);
	// Initialize the servo
	//jaw_servo = new Servo();
	//jaw_servo->attach(SERVO_CONTROL_PIN, SERVO_PWM_MIN, SERVO_PWM_MAX);
	//servo.attachPin(SERVO_CONTROL_PIN, 1000, 10);
	servo = pwmFactory(SERVO_CONTROL_PIN);
	if(servo == NULL) {
		// Servo not found
		servo = new ESP32PWM();
	}
	// Configure the servo
	servo->attachPin(SERVO_CONTROL_PIN, 100, 10);
}

Robot::~Robot() {
	// Ignore this
}

void Robot::loop() {
	// Robot Loop

	//Serial.printf("Current State: %i\n", this->robot_state);

	switch(this->robot_state) {
		case(MOVE_TO_PICKUP):
			Serial.println("A");
			if(this->pm1->within(30) && this->pm2->within(30)) {
				// Done!
				Serial.println("Ready to grab!");
				this->robot_state = READY_TO_GRAB;
			}
			break;
		case(MOVING_TO_DROP_OFF):
			if(this->sub_state == 0) {
				if(this->pm1->within(30) && this->pm2->within(30) && this->arm->within(30)) {
					// Finished with first stage
					Serial.println("Finished first stage of moving to drop off");
					// Update position
					this->pm1->reset_position();
					this->pm2->reset_position();
					this->pm1->setpoint = 1200*-2;
					this->pm2->setpoint = 1200*2;
					this->sub_state = 1;
				}
			} else if(this->sub_state == 1) {
				if(this->pm1->within(30) && this->pm2->within(30) && this->arm->within(30)) {
					// Finished with second stage
					Serial.println("Finished second stage of moving to drop off");
					// Update position
					this->pm1->reset_position();
					this->pm2->reset_position();
					//this->arm->reset_position();
					this->pm1->setpoint = 1200*5;
					this->pm2->setpoint = 1200*5;
					//this->arm->setpoint = 1200*2;
					this->sub_state = 2;
				}
			} else if(this->sub_state == 2) {
				if(this->pm1->within(30) && this->pm2->within(30) && this->arm->within(30)) {
					// Finished with first stage
					Serial.println("Finished first stage of moving to drop off");
					// Update stage
					this->robot_state = READY_TO_DROP_OFF;
				}
			} else {
				Serial.println("INVALID SUB STATE FOR MOVING_TO_DROP_OFF!!!");
			}
			break;
		default:
			break;
	}

	// Update PID motors
	this->pm1->loop();
	this->pm2->loop();
	this->arm->loop();

	delay(100);
	Serial.println("");
}

void Robot::approve() {
	// Check the state
	switch(this->robot_state) {
		case(READY_TO_RUMBLE):
			// Open jaw
			//this->jaw_servo->write(90);
			this->pm1->reset_position();
			this->pm2->reset_position();
			this->pm1->setpoint = 1200*100;
			this->pm2->setpoint = 1200*100;
			this->robot_state = MOVE_TO_PICKUP;
			break;
		case(READY_TO_GRAB):
			// Close the servo
			//this->servo->adjustFrequency(1000, 0.1);
			// Delay for a smidge
			delay(500);
			// Change state
			this->robot_state = READY_TO_MOVE_TO_DROP_OFF;
			break;
		case(READY_TO_MOVE_TO_DROP_OFF):
			// Head on over
			this->sub_state = 0;
			this->pm1->reset_position();
			this->pm2->reset_position();
			this->pm1->setpoint = 1200*3;
			this->pm2->setpoint = 1200*3;
			// Change the state
			this->robot_state = MOVING_TO_DROP_OFF;
			break;
		default:
			// Increment state regardless
			this->robot_state = (RobotState)((int)this->robot_state + 1);
			break;

	}
}

void Robot::estop() {
	// Stop the motors
	this->pm1->emergency_stop = true;
	this->pm2->emergency_stop = true;
	this->arm->emergency_stop = true;
}

void Robot::resume() {
	// Resume the motors
	this->pm1->emergency_stop = false;
	this->pm2->emergency_stop = false;
	this->arm->emergency_stop = false;
}

void Robot::start() {
	// Lets get started
	this->robot_state = READY_TO_RUMBLE;
}
