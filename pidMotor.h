/*!
 * 	@file src/robot/pidMotor.h
 *
 * 	This file contains the deifnition for the pidmotor class and it's related
 * 	statics and variables.
 */

#ifndef _PIDMOTOR_H_
#define _PIDMOTOR_H_

#include "Arduino.h"

#include "analogWrite.h"

#define private public
#include <PID_v1.h>
#undef private

#define PIDMOTOR_DEBUG_GRAPH_PRINTING

/*!
 * 	The PidMotor class represents an instance of a motor and a pid loop
 * 	combined together into a single, easy to use object.
 */
class PidMotor {

	public:
		/// Motor Input Value
		volatile double input_value = 0;
		/// PID Input value
		double pid_input_value = 0;
		/// PID output value
		double output_value = 0;

		/// Motor FWD pin
		char motor_f = 0;
		/// Motor REV pin
		char motor_r = 0;

		/// Encoder A pin
		char enc_a = 0;
		/// Encoder B pin
		char enc_b = 0;

		/// Emergency stop flag
		bool emergency_stop = false;

		/// Encoder interrupt handler
		void IRAM_ATTR encoder_handle();

		/// Instance ID
		uint8_t instance_id;
		// Instance ID tracker
		static uint8_t instance_id_tracker;

		/// PID Object
		PID* pid = nullptr;

		// Encoder previous value
		uint8_t enc_prev = 0b00;

		/// PidMotor static references
		static PidMotor* static_pref_1;
		static PidMotor* static_pref_2;
		static PidMotor* static_pref_3;
		/// PidMotor interrupt static handlers
		static void IRAM_ATTR int_hand_1();
		static void IRAM_ATTR int_hand_2();
		static void IRAM_ATTR int_hand_3();

	public:
		/// Setpoint
		double setpoint = 0;

		/*!
		 * 	PidMotor constructor.
		 *
		 * 	This creates and initializes a PID loop object thing.  It also does
		 * 	the stuff to make the motors do a go.
		 *
		 * 	@param[in]	motor_f	Pin for the forward input on the h-bridge
		 * 	@param[in]	motor_r	Pin for the reverse input on the h-bridge
		 * 	@param[in]	enc_a	Motor encoder pole A
		 * 	@param[in]	enc_b	Motor encoder pole B
		 * 	@param[in]	spd_max	Max motor speed
		 * 	@param[in]	spd_min	Min motor speed
		 */
		PidMotor(char motor_f, char motor_r, char enc_a, char enc_b, double spd_min, double spd_max);

		/*!
		 * 	Update the motor.
		 *
		 * 	This method should be called as frequently as possible, as it is
		 * 	responsible for updating the pid loop and motor speed.
		 */
		void loop();

		/*!
		 * 	Update PID configuration.
		 */
		void update(double p, double i, double d);

		/*!
		 *  Emergency Stop.
		 */
		void estop();

		/*!
		 * 	Set the motor speed.
		 */
		void set_speed(double speed);

		/*!
		 * 	Reset motor position.
		 *
		 * 	This method will set the position and setpoint to zero.  Calling
		 * 	this method while the robot is in motion will cause it to abruptly
		 * 	halt.
		 */
		void reset_position();

		/*!
		 *  Check if robot within target.
		 */
		bool within(int target);

};

#endif
