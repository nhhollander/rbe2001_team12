#include "wifi.hpp"

#define REMOTE_ADDR this->udp.remoteIP().toString().c_str()

/*!
 *  Command handler case macro
 */
#define CMD_CASE(c) case(c): Serial.println("Got command "#c); \


/*!
 * 	Command Definitions
 */
#define DEBUG_COMMAND 0xaf
#define PING_COMMAND 0xc0
#define START_COMMAND 0xc1
#define ESTOP_COMMAND 0xc2
#define RESUME_COMMAND 0xc3
#define APPROVE_COMMAND 0xc4
#define SET_PID 0xc5
#define OPEN_JAW 0xd0
#define CLOSE_JAW 0xd1
#define ARM_DOWN 0xe0
#define ARM_UP 0xe1
#define PICK_CMD 0xf0
#define MOVE_CMD 0xf1

WifiRemote::WifiRemote(Robot* robot, const char* ssid, const char* password) {
	// Save the target robot
	this->robot = robot;
	// Connect to the wifi network
	Serial.print("Connecting to ");
	Serial.println(ssid);
	WiFi.begin(ssid, password);
	while(WiFi.status() != WL_CONNECTED) {
		Serial.print(".");
		delay(500);
	}
	// Connected!
	Serial.println("\nConnected!");
	Serial.print("IP Address: ");
	Serial.println(WiFi.localIP());
	delay(2000);
	// Create the command listener
	this->udp.begin(420);

}

struct __attribute__((packed)) setpid_msg {
	char msg_id;
	char pid_target;
	double p;
	double i;
	double d;
};

struct __attribute__((packed)) pick_msg {
    char msg_id;
    char angle;
    char position;
};

struct __attribute__((packed)) move_msg {
    char msg_id;
    char direction;
    double count;
};

// Jaw Position
double jpos;

void WifiRemote::check_mail() {

	// Packet size
	int packet_size = this->udp.parsePacket();
	// Check if message available
	if(packet_size > 0) {
		// We've got a message!
		Serial.printf("Received %d bytes from %s\n", packet_size, REMOTE_ADDR);
		// Parse the message
		int msg_len = this->udp.read(this->msg_buffer, MSG_BUFFER_SIZE);
		// Add a null terminator to the message
		msg_buffer[msg_len] = '\0';
		// Check the message token
		switch(msg_buffer[0]) {
			CMD_CASE(DEBUG_COMMAND)
				Serial.println("DEBUG COMMAND!!!!");
				delay(2000);
				break;
			CMD_CASE(PING_COMMAND)
				// TODO: pong?
				break;
			CMD_CASE(START_COMMAND)
				this->robot->start();
				break;
			CMD_CASE(ESTOP_COMMAND)
				// Stop the robot
				this->robot->estop();
				break;
			CMD_CASE(RESUME_COMMAND)
				// Resume the robot
				this->robot->resume();
				break;
			CMD_CASE(APPROVE_COMMAND)
				// Continue
				this->robot->approve();
				break;
			CMD_CASE(SET_PID) {
				// Get the pid message
				setpid_msg* pidmsg = (setpid_msg*) &msg_buffer[0];
				// Set the pid
				if(pidmsg->pid_target == 0) {
					this->robot->pm1->update(pidmsg->p, pidmsg->i, pidmsg->d);
				} else if(pidmsg->pid_target == 1) {
					this->robot->pm2->update(pidmsg->p, pidmsg->i, pidmsg->d);
				} else if(pidmsg->pid_target == 2) {
					this->robot->arm->update(pidmsg->p, pidmsg->i, pidmsg->d);
				} else {
					// INVALID!
					Serial.println("INVALID PID TARGET!");
				}
				Serial.printf("Got P: %lf I: %lf D: %lf\n", pidmsg->p, pidmsg->i, pidmsg->d);
				//delay(500);
				break;
			}
			break;
			CMD_CASE(OPEN_JAW)
				jpos = .1;
				this->robot->servo->adjustFrequency(100, .1);
				break;
			CMD_CASE(CLOSE_JAW)
				jpos = .145;
				this->robot->servo->adjustFrequency(100, .145);
				break;
			CMD_CASE(ARM_DOWN)
				this->robot->arm->reset_position();
<<<<<<< HEAD
				this->robot->arm->setpoint = 1200*3;
				this->robot->pm1->reset_position();
				this->robot->pm2->reset_position();
				break;
			CMD_CASE(ARM_UP)
				this->robot->arm->reset_position();
				this->robot->arm->setpoint = -1200*3;
				this->robot->pm1->reset_position();
				this->robot->pm2->reset_position();
				break;
			CMD_CASE(PICK_CMD) {
				// Get the pick message
				pick_msg* pickmsg = (pick_msg*) &msg_buffer[0];
				// Check the pick details
				bool bad_data = false;
				if(pickmsg->angle > 1) {
					// Angle out of range
					Serial.println("Invalid angle received!");
					bad_data = true;
				}
				if(pickmsg->position > 1) {
					// Position out of range
					Serial.println("Position out of range!");
					bad_data = true;
				}
				// Save the good data
				if(!bad_data) {
					this->robot->angle = pickmsg->angle;
					this->robot->position = pickmsg->position;
					Serial.printf("Set angle to %i degrees and Position to %i\n",
						(pickmsg->angle == 0) ? 45 : 25,
						(pickmsg->position == 0) ? 1 : 2);
				} else {

				}
				break;
			}
			break;
			CMD_CASE(MOVE_CMD) {
				// Get the move message
				move_msg* movemsg = (move_msg*) &msg_buffer[0];
				// Get the target
				//this->robot->pm1->reset_position();
				//this->robot->pm2->reset_position();
				this->robot->pm1->setpoint += movemsg->count;
				this->robot->pm2->setpoint += movemsg->count;
				Serial.printf("Moving %lf steps forward\n", movemsg->count);
			}
			break;
			default:
				Serial.printf("Unknown command \"%02X\"\n", msg_buffer[0]);
				break;
		}
	}

	// Update frqeuency
	//this->robot->servo.adjustFrequency(100, jpos);

}
