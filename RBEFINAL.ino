/*!
 *  RBE 2001 Final Project.
 *
 *  This shiny new firmware was written the night before the project due date
 *  because the wifi stopped working, and sometimes the easiest way to fix a
 *  problem is to nuke it from orbit.
 */

#include "robot.hpp"
#include "wifi.hpp"

// Contains the network ssid and password
#include "wifi_credentials.h"

/// The robot
Robot* robot = nullptr;
/// The wifi remote control interface
WifiRemote* wifi = nullptr;

// the setup function runs once when you press reset or power the board
void setup() {

	// Initialize Serial
	Serial.begin(115200);

	// Create the robot
	robot = new Robot();
	// Create the wifi control interface
	wifi = new WifiRemote(robot, wifi_ssid, wifi_pass);
}

// the loop function runs over and over again forever
void loop() {
	// Check for mail
	wifi->check_mail();
	// Invoke the robot main loop
	robot->loop();
}
