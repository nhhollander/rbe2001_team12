#include "pidMotor.h"

// Statics
PidMotor* PidMotor::static_pref_1 = nullptr;
PidMotor* PidMotor::static_pref_2 = nullptr;
PidMotor* PidMotor::static_pref_3 = nullptr;
uint8_t PidMotor::instance_id_tracker = 0;


PidMotor::PidMotor(char motor_f, char motor_r, char enc_a, char enc_b, double spd_min, double spd_max) {
	// Create the PID instance
	pid = new PID(&this->pid_input_value, &this->output_value, &this->setpoint, 0, 0, 0, DIRECT);
	// Configure pid
	pid->SetOutputLimits(spd_min, spd_max);
	pid->SetMode(1); // What does this do?  Why does the code not work without it?
	// Save variables
	this->motor_f = motor_f;
	this->motor_r = motor_r;
	this->enc_a = enc_a;
	this->enc_b = enc_b;
	// Configure the motor IO
	pinMode(motor_f, OUTPUT);
	pinMode(motor_r, OUTPUT);
	// Configure the interrupt handlers
	pinMode(enc_a, INPUT_PULLDOWN);
	pinMode(enc_b, INPUT_PULLDOWN);
	// Find the interrupt handler
	if(static_pref_1 == nullptr) {
		static_pref_1 = this;
		attachInterrupt(digitalPinToInterrupt(enc_a), this->int_hand_1, CHANGE);
		attachInterrupt(digitalPinToInterrupt(enc_b), this->int_hand_1, CHANGE);
	} else if(static_pref_2 == nullptr) {
		static_pref_2 = this;
		attachInterrupt(digitalPinToInterrupt(enc_a), this->int_hand_2, CHANGE);
		attachInterrupt(digitalPinToInterrupt(enc_b), this->int_hand_2, CHANGE);
	} else if(static_pref_3 == nullptr) {
		static_pref_3 = this;
		attachInterrupt(digitalPinToInterrupt(enc_a), this->int_hand_3, CHANGE);
		attachInterrupt(digitalPinToInterrupt(enc_b), this->int_hand_3, CHANGE);
	} else {
		// Too many pid motors!  Abandon ship!
		while(true) {
			Serial.println("TOO MANY PID MOTORS!!!");
		}
	}
	// Set instance id
	this->instance_id = PidMotor::instance_id_tracker++;
}

// Static interrupt handlers
void IRAM_ATTR PidMotor::int_hand_1() { PidMotor::static_pref_1->encoder_handle(); }
void IRAM_ATTR PidMotor::int_hand_2() { PidMotor::static_pref_2->encoder_handle(); }
void IRAM_ATTR PidMotor::int_hand_3() { PidMotor::static_pref_3->encoder_handle(); }

// PidMotor interrupt handler
void IRAM_ATTR PidMotor::encoder_handle() {
	// Get the encoder state
	uint8_t enc_now = digitalRead(this->enc_b) << 1;
	enc_now |= digitalRead(this->enc_a);
	// Check previous state
	if(this->enc_prev == 0b00) {
		if(enc_now == 0b10) {
			this->input_value++;
		} else if(enc_now == 0b01) {
			this->input_value--;
		}
	} else if(this->enc_prev == 0b01) {
		if(enc_now == 0b00) {
			this->input_value++;
		} else if(enc_now == 0b11) {
			this->input_value--;
		}
	} else if(this->enc_prev == 0b10) {
		if(enc_now == 0b11) {
			this->input_value++;
		} else if(enc_now == 0b00) {
			this->input_value--;
		}
	} else if(this->enc_prev == 0b11) {
		if(enc_now == 0b01) {
			this->input_value++;
		} else if(enc_now == 0b10) {
			this->input_value--;
		}
	}
	// Save the value
	this->enc_prev = enc_now;
}

void PidMotor::loop() {
	// Update input value
	this->pid_input_value = this->input_value;
	// Update the PID loop magic
	if(!this->pid->Compute() && false) {
		// Failure
		Serial.println("PID FAILURE");
		while(true);
	}
	// Make sure emergency stop flag is not set
	if(!this->emergency_stop) {
		// Update the motor speed
		if(this->output_value > 0) {
			// Forward
			analogWrite(this->motor_r, 0);
			analogWrite(this->motor_f, (uint8_t) this->output_value);
		} else {
			// Reverse
			analogWrite(this->motor_f, 0);
			analogWrite(this->motor_r, (uint8_t) (this->output_value * -1.0));
		}
	} else {
		// Stop the motor
		analogWrite(this->motor_r, 0);
		analogWrite(this->motor_f, 0);
	}
	// Debug graph printing
	#undef PIDMOTOR_DEBUG_GRAPH_PRINTING
	#ifdef PIDMOTOR_DEBUG_GRAPH_PRINTING
		// Create double conversion union
	//if(this->instance_id != 2) { return; }
		Serial.print("PID: ");
		Serial.print(this->instance_id);
		Serial.print(" ");
		Serial.print(this->input_value);
		Serial.print(" ");
		Serial.print(this->setpoint);
		Serial.print(" ");
		Serial.print(this->pid->kp);
		Serial.print(" ");
		Serial.print(this->pid->ki);
		Serial.print(" ");
		Serial.print(this->pid->kd);
		Serial.print(" ");
		Serial.println(this->output_value);
	#endif
}

void PidMotor::update(double p, double i, double d) {
	// Update
	this->pid->SetTunings(p,i,d);
}

void PidMotor::estop() {
	// Stop the motor
	analogWrite(this->motor_f, 0);
	analogWrite(this->motor_r, 0);
	// Set the stop flag
	this->emergency_stop = true;
}

void PidMotor::set_speed(double speed) {
	// Set the speed
	this->pid->SetOutputLimits(-speed, speed);
}

void PidMotor::reset_position() {
	// Reset the input value
	this->input_value = 0;
	// Reset the setpoint
	this->setpoint = 0;
}


bool PidMotor::within(int target) {
	return abs(this->input_value - this->setpoint) < target;
}
